﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_7
{
    delegate void ProcessOrder();
    internal class Coffeemaker
    {  
        public  void makeCoffee(ProcessOrder handler)
        {

            Console.WriteLine("make coffee");
            handler();
        }
    }
}
