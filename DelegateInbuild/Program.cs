﻿// See https://aka.ms/new-console-template for more information
namespace Inbuild_Delegate;

class Myclass
{
    static void Main()
    {

        Action<int> sq1 = a => Console.WriteLine($"square is:{a*a}");
        sq1(11);
        Func<int, int> sq2 = b => b * b;
        Console.WriteLine(sq2(12));
        Predicate<int> mynumber = a => a > 0 ? true : false;
        Console.WriteLine("enter number");
        int num = int.Parse(Console.ReadLine());
        var result = mynumber(num);
        if (result)
        {
            Console.WriteLine("poaitive number");
        }
        else
        {
            Console.WriteLine("negative number");
        }
    }
}
