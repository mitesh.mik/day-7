﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLINQ.Model
{
    internal class Product
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }

        public string Make { get; set; }

        public override string ToString()
        {
            return $"Name:{Name} \t Category:{Category} \t Price:{Price} \t Make:{Make}";
        }
    }
}
