﻿// See https://aka.ms/new-console-template for more information

using DemoLINQ.Model;

Product[] product = new Product[]
{
    new Product(){Name="nokia" ,Category="mobile", Price=5000 ,Make="Nokia"},
      new Product(){Name="apple" ,Category="mobile", Price=10000 ,Make="Apple"},
        new Product(){Name="realmi" ,Category="tv", Price=20000 ,Make="Realmi"},

};

Console.WriteLine("using method LINQ");
Product[] products=product.Where(p=>p.Price>5000 && p.Price<20000).ToArray();
foreach(Product p in products)
{
    Console.WriteLine(p);
}
Console.WriteLine("query syntax");
var filterpoduct = (from p in product where p.Price > 5000 && p.Price < 20000 select p).ToArray();
foreach (Product p in filterpoduct)
{
    Console.WriteLine(p);
}