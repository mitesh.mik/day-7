﻿// See https://aka.ms/new-console-template for more information
using Task_7.Model;

List<Product> products = new List<Product>() {
new Product(){Id=1,Name="Lenovo"},
new Product(){Id=2,Name="Redmi"},
new Product(){Id=3,Name="Apple"},
new Product(){Id=4,Name="Realmi"},

};

List<ShoppingProduct> shoppings = new List<ShoppingProduct>
{
    new ShoppingProduct(){Id=1,Quanity=2},
    new ShoppingProduct(){Id=2,Quanity=3},
    new ShoppingProduct(){Id=3,Quanity=4},
    new ShoppingProduct(){Id=4,Quanity=5},


};


var result = from p in products
             join sp in shoppings
           on p.Id equals sp.Id
             select new
             {   productId=p.Id,
                 productName = p.Name,
                 quantities = sp.Quanity
             };

foreach (var item in result)
{
    Console.WriteLine(item);
}
