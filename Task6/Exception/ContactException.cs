﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6.Exception
{
    internal class ContactException:ApplicationException
    {
        public ContactException()
        {

        }
        public ContactException(string Message):base(Message)
        {

        }
    }
}
