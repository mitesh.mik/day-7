﻿// See https://aka.ms/new-console-template for more information
using Task6.Exception;
using Task6.Module;
using Task6.Repository;

ContactRepository contactrepo = new ContactRepository();

Console.WriteLine("get all contacts");
List<Contact> mycontact=contactrepo.getAllContacts();
foreach(Contact contact in mycontact)
{
    Console.WriteLine(contact);
}
Console.WriteLine("Add contact");
Contact contactperson = new Contact();
//{ Name="ilyas",Address="panvel",City="navi mumbai",PhoneNumber=234678912};
Console.WriteLine("enter name");
string myname=Console.ReadLine();
Console.WriteLine("enter Address");
string myadd = Console.ReadLine();
Console.WriteLine("enter city");
string mycity = Console.ReadLine();
Console.WriteLine("enter phone number");
double mynumber = double.Parse(Console.ReadLine());
contactperson.Name = myname;
contactperson.Address = myadd;
contactperson.City = mycity;
contactperson.PhoneNumber = mynumber;
try
{
    contactrepo.addContact(contactperson);

}
catch(ContactException ce)
{
    Console.WriteLine(ce.Message);
}

Console.WriteLine("delete by name");
string personName=Console.ReadLine();
bool result=contactrepo.deleteContactByName(personName);
if (result)
{
    Console.WriteLine("contact deleted");
}
else
{
    Console.WriteLine("contact not deleted");
}
Console.WriteLine("update contact");
string ContactName = Console.ReadLine();
contactrepo.updateContact(ContactName);

Console.WriteLine("find by city");
string cityname = Console.ReadLine();
contactrepo.getContactByCity(cityname);
