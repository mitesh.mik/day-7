﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task6.Exception;
using Task6.Module;

namespace Task6.Repository
{
    internal class ContactRepository
    {
        List<Contact> contacts;
        public ContactRepository()
        {
            contacts = new List<Contact>()
            {
                new Contact(){ Name = "mitesh", Address="Titwala", City="Mumbai", PhoneNumber=234678912},
                new Contact(){Name="sanket", Address="Neral", City="Mumbai", PhoneNumber=2345678912}
            };
        }

        public void addContact(Contact con)
        {
            var getcontact=getContactByName(con.Name);
            if (getcontact == null)
            {
                contacts.Add(con);
                Console.WriteLine("contact added successfully");
            }
            else
            {
                throw new ContactException("contact is already exist");
                
            }

        }

        public Contact getContactByName(string name)
        {
           return contacts.FirstOrDefault(item=>item.Name==name);
        }
        public List<Contact> getAllContacts()
        {
            return contacts;
        }

        public bool deleteContactByName(string name)
        {
            var delName = getContactByName(name);
            return delName != null ? contacts.Remove(delName) : false;
        }

        public void updateContact(string name)
        {
            var upContact=contacts.FirstOrDefault(item=>item.Name==name);
            Console.WriteLine("enter address");
            string add = Console.ReadLine();
            Console.WriteLine("enter city");
            string city = Console.ReadLine();
            Console.WriteLine("enter phone number");
            double phn = double.Parse(Console.ReadLine());

            upContact.Address = add;
            upContact.City = city;
            upContact.PhoneNumber=phn;
            foreach(Contact person in contacts)
            {
                Console.WriteLine(person);
            }
        }
        public void getContactByCity(string cityname)
        {
            var cities= contacts.Where(item => item.City == cityname);

            //var cities=from p in contacts group p by p.City==cityname;
            //foreach (var item in cities)
            //{
            //    Console.WriteLine(item.Key);
            //    foreach(var item1 in item)
            //    {
            //        Console.WriteLine(item1);
            //    }

            //}
            foreach(Contact person in cities)
            {
                Console.WriteLine(person);
            }


        }

    }
}
