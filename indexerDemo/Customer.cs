﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace indexerDemo
{
    internal class Customer
    {
        int id;
        string name;
        string city;

       

        public Customer(int id, string name, string city)
        {
            this.id = id;
            this.name = name;
            this.city = city;
        }
        //indexers
        public object this[int index]
        {
            get
            {
                if (index == 1) return id;
                else if (index == 2) return name;
                else if (index == 3) return city;
                else return null;
            }
            set
            {

                if(index==1) id = (int)value;
                else if (index==2) name = (string)value;
                else if(index==3) city = (string)value;


            }
        }
    }
}
